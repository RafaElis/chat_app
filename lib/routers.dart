import 'package:chat_app/controllers/auth_controller.dart';
import 'package:chat_app/pages/chat_page.dart';
import 'package:chat_app/pages/home_page.dart';
import 'package:chat_app/pages/login_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

goRouteBuilder(WidgetRef ref) => GoRouter(
      initialLocation: HomePage.routePath,
      routes: [
        GoRoute(
          path: HomePage.routePath,
          name: HomePage.routeName,
          pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey,
            child: const HomePage(),
          ),
        ),
        GoRoute(
          path: LoginPage.routePath,
          name: LoginPage.routeName,
          pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey,
            child: const LoginPage(),
          ),
        ),
        GoRoute(
          path: ChatPage.routePath,
          name: ChatPage.routeName,
          pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey,
            child: const ChatPage(),
          ),
        ),
      ],
      redirect: (state) {
        final User? authInformation = ref.watch(authControllerProvider);
        final bool loggedIn = authInformation?.uid != null;
        final loggingIn = state.subloc == '/login';
        if (!loggedIn) return loggingIn ? null : '/login';
        return null;
      },
      errorPageBuilder: (context, state) => MaterialPage(
        key: state.pageKey,
        child: Scaffold(
          body: Center(
            child: Text(
              state.error.toString(),
            ),
          ),
        ),
      ),
    );
