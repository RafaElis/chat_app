import 'dart:async';

import 'package:chat_app/repositories/auth_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final authControllerProvider = StateNotifierProvider<AuthController, User?>(
  (ref) => AuthController(ref.read),
);

class AuthController extends StateNotifier<User?> {
  final Reader _reader;
  StreamSubscription<User?>? _authStateChangeSubscription;

  AuthController(this._reader) : super(null) {
    _authStateChangeSubscription?.cancel();
    _authStateChangeSubscription = _reader(authRepositoryProvider)
        .authStateChanges
        .listen((user) => state = user);
  }

  @override
  void dispose() {
    _authStateChangeSubscription?.cancel();
    super.dispose();
  }

  void appStarted() async {
    final user = _reader(authRepositoryProvider).getCurrentUser();
  }

  Future<void> signOut() async {
    await _reader(authRepositoryProvider).signOut();
    state = null;
  }

  Future<void> signInWithEmailAndPassword(String email, String password) async {
    await _reader(authRepositoryProvider)
        .signInWithEmailAndPassword(email, password);
  }

  Future<void> signUpWithEmailAndPassword(String email, String password) async {
    await _reader(authRepositoryProvider)
        .signUpWithEmailAndPassword(email, password);
  }
}
