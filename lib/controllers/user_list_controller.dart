import 'package:chat_app/controllers/auth_controller.dart';
import 'package:chat_app/models/user_model.dart';
import 'package:chat_app/repositories/custom_exception.dart';
import 'package:chat_app/repositories/user_repository.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:flutter_riverpod/flutter_riverpod.dart';

final userListExceptionProvider = StateProvider<CustomException?>((_) => null);

final userListControllerProvider = StateNotifierProvider((ref) {
  final firebase_auth.User? user = ref.watch(authControllerProvider);
  return UserListController(ref.read, user?.uid);
});

class UserListController extends StateNotifier<AsyncValue<List<User>>> {
  final Reader _reader;
  final String? _userId;

  UserListController(this._reader, this._userId)
      : super(const AsyncValue.loading()) {
    if (_userId != null) {
      getUsers();
    }
  }

  Future<void> getUsers({bool isLoading = false}) async {
    if (isLoading) state = const AsyncValue.loading();
    try {
      final List<User> users =
          await _reader(userRepositoryProvider).getUsers(_userId!);
      if (mounted) {
        state = AsyncValue.data(users);
      }
    } on CustomException catch (e, st) {
      state = AsyncValue.error(
        e,
        stackTrace: st,
      );
    }
  }
}
