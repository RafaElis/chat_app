import 'package:chat_app/controllers/auth_controller.dart';
import 'package:chat_app/controllers/user_list_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ChatPage extends ConsumerWidget {
  static const String routePath = '/chat';
  static const String routeName = 'chat';

  const ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Demo Home Page'),
        actions: [
          IconButton(
            icon: const Icon(Icons.exit_to_app),
            onPressed: () {
              ref.read(authControllerProvider.notifier).signOut();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            StreamProvider.value(
              value: ref.read(userListControllerProvider.notifier).getUsers(),
              initialData: null,
              child: const SizedBox(height: 48.0),
            ),
          ],
        ),
      ),
    );
  }
}
