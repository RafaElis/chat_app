import 'package:chat_app/constants/constants.dart';
import 'package:chat_app/controllers/auth_controller.dart';
import 'package:chat_app/controllers/user_list_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final userListProvider = FutureProvider<void>((ref) async {
  return await ref
      .read(userListControllerProvider.notifier)
      .getUsers(isLoading: true);
});

class HomePage extends ConsumerWidget {
  static const String routePath = '/';
  static const String routeName = 'home';
  const HomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    AsyncValue<void> users = ref.watch(userListProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Demo Home Page'),
        actions: [
          IconButton(
            icon: const Icon(Icons.exit_to_app),
            onPressed: () {
              ref.read(authControllerProvider.notifier).signOut();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            users.when(
              loading: () => const CircularProgressIndicator(),
              error: (err, stack) => Text('Error: $err'),
              data: (data) {
                return ListView.builder(
                  physics: const BouncingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: 8,
                  padding: const EdgeInsets.all(12),
                  itemBuilder: (context, index) {
                    return Container(
                      color: Colors.red,
                      child: const ListTile(
                        leading: Icon(Icons.person, size: 34),
                        title: Text(
                          'users.name',
                          style: kTextFieldStyle,
                        ),
                        subtitle: Text(
                          'users.email',
                          style: kTextFieldStyle,
                        ),
                        trailing: Icon(Icons.chevron_right_sharp),
                      ),
                    );
                  },
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
