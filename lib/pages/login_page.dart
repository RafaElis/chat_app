import 'package:chat_app/constants/app_images.dart';
import 'package:chat_app/constants/constants.dart';
import 'package:chat_app/controllers/auth_controller.dart';
import 'package:chat_app/pages/chat_page.dart';
import 'package:chat_app/widgets/button.dart';
import 'package:chat_app/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

class LoginPage extends ConsumerStatefulWidget {
  static const String routePath = '/login';
  static const String routeName = 'login';

  const LoginPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends ConsumerState<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 200.0,
              child: Image.asset(AppImages.logo),
            ),
            const SizedBox(height: 48.0),
            TextInput(
              controller: _emailController,
              hintText: 'Enter your email',
              type: FieldsType.email,
              color: Colors.lightBlueAccent,
            ),
            const SizedBox(height: 8.0),
            TextInput(
              controller: _passwordController,
              hintText: 'Enter your password',
              type: FieldsType.password,
              color: Colors.lightBlueAccent,
            ),
            const SizedBox(height: 24.0),
            Button(
              'Log In',
              color: Colors.lightBlueAccent,
              onPressed: () => _handleLogin(context),
            ),
          ],
        ),
      ),
    );
  }

  void _handleLogin(BuildContext context) async {
    final auth = ref.read(authControllerProvider.notifier);
    try {
      setState(() {
        _isLoading = true;
      });

      await auth.signInWithEmailAndPassword(
        _emailController.text,
        _passwordController.text,
      );
      if (mounted) {
        context.pushNamed(ChatPage.routeName);
      }
      setState(() {
        _isLoading = false;
      });
    } on Exception catch (e) {
      print(e);
      rethrow;
    }
  }
}
