class CustomException implements Exception {
  final String? message;

  const CustomException(
      {this.message = 'Tivemos um problema, tente novamente mais tarde.'});

  @override
  String toString() => 'CustomException: $message';
}
