import 'package:chat_app/general_providers.dart';
import 'package:chat_app/models/user_model.dart';
import 'package:chat_app/repositories/custom_exception.dart';
import 'package:firebase_auth/firebase_auth.dart' hide User;
import 'package:flutter_riverpod/flutter_riverpod.dart';

abstract class BaseUserRepository {
  Future<List<User>> getUsers(String userId);
  Future<void> updateUser(User user);
  Future<void> updateLastMessageTime(String userId, DateTime time);
}

final userRepositoryProvider =
    Provider<UserRepository>((ref) => UserRepository(ref.read));

class UserRepository implements BaseUserRepository {
  final Reader _reader;

  UserRepository(this._reader);

  @override
  Future<List<User>> getUsers(String userId) async {
    try {
      final response = _reader(firebaseFirestoreProvider).collection('users');
      final users = await response.where('id', isNotEqualTo: userId).get();
      return users.docs.map((doc) => User.fromJson(doc.data())).toList();
    } on FirebaseException catch (e) {
      throw CustomException(message: e.message);
    }
  }

  @override
  Future<void> updateUser(User user) async {
    try {
      await _reader(firebaseFirestoreProvider)
          .collection('users')
          .doc(user.id)
          .update(user.toDocument());
    } on FirebaseException catch (e) {
      throw CustomException(message: e.message);
    }
  }

  @override
  Future<void> updateLastMessageTime(String userId, DateTime time) async {}
}
