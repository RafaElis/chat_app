import 'package:chat_app/constants/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class MessageInput extends StatelessWidget {
  final TextEditingController messageController;
  final User loggedInUser;
  final VoidCallback sendMessage;
  const MessageInput({
    required this.messageController,
    required this.loggedInUser,
    required this.sendMessage,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      child: TextField(
        controller: messageController,
        style: kTextFieldStyle,
        decoration: kTextFieldDecoration.copyWith(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: Colors.lightBlueAccent,
              width: 1.0,
            ),
            borderRadius: BorderRadius.circular(0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: Colors.lightBlueAccent,
              width: 2.0,
            ),
            borderRadius: BorderRadius.circular(0),
          ),
          hintText: 'Enter a message...',
          suffixIcon: Material(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(16),
            child: InkWell(
              borderRadius: BorderRadius.circular(16),
              splashColor: Colors.blueAccent,
              onTap: sendMessage,
              child: const Icon(Icons.send),
            ),
          ),
        ),
      ),
    );
  }
}
