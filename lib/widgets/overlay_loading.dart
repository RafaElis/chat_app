import 'package:flutter/material.dart';

class OverlayLoading extends StatefulWidget {
  final Widget child;
  final bool isLoading;

  const OverlayLoading({Key? key, required this.child, required this.isLoading})
      : super(key: key);

  // const OverlayLoading({
  //   required this.child,
  //   required this.isLoading,
  //   super.key,
  // });

  @override
  State<OverlayLoading> createState() => _OverlayLoadingState();
}

class _OverlayLoadingState extends State<OverlayLoading> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        widget.child,
        if (widget.isLoading)
          Container(
            color: Colors.black.withOpacity(0.5),
            alignment: Alignment.center,
            child: const CircularProgressIndicator.adaptive(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
            ),
          ),
      ],
    );
  }
}
