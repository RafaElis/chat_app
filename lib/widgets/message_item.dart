import 'package:chat_app/constants/constants.dart';
import 'package:chat_app/constants/utility.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MessageItem extends StatelessWidget {
  const MessageItem({
    Key? key,
    required this.text,
    required this.uid,
    required this.time,
    required this.isMe,
  }) : super(key: key);

  final String text;
  final String uid;
  final Timestamp time;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        // mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 20.0,
            ),
            decoration: BoxDecoration(
              color: isMe ? Colors.red : Colors.blue,
              borderRadius: BorderRadius.only(
                topLeft: isMe ? const Radius.circular(15.0) : Radius.zero,
                topRight: !isMe ? const Radius.circular(15.0) : Radius.zero,
                bottomLeft: const Radius.circular(15.0),
                bottomRight: const Radius.circular(15.0),
              ),
            ),
            child: Text(
              text,
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: Text(
              Utility.getFormattedDateTime(
                time.toDate().toString(),
                pattern: 'Hm',
              ),
              style: kTextFieldStyle,
            ),
          ),
        ],
      ),
    );
  }
}
