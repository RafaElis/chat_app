import 'package:chat_app/constants/constants.dart';
import 'package:flutter/material.dart';

class TextInput extends StatelessWidget {
  final String hintText;
  final TextEditingController controller;
  final FieldsType type;
  final Color color;

  const TextInput(
      {Key? key,
      required this.hintText,
      required this.controller,
      required this.type,
      required this.color})
      : super(key: key);

  // const TextInput({
  //   required this.controller,
  //   required this.hintText,
  //   required this.type,
  //   required this.color,
  //   super.key,
  // });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: selectFieldType(type)['keyboardType'],
      // keyboardType: type.keyboardType,
      style: kTextFieldStyle,
      obscureText: selectFieldType(type)['obscureText'],
      decoration: kTextFieldDecoration.copyWith(
        hintText: 'Enter your email',
        prefixIcon: Icon(selectFieldType(type)['icon']),
        // prefixIcon: Icon(type.icon),
        prefixIconColor: color,
        enabledBorder: kTextFieldDecoration.enabledBorder?.copyWith(
          borderSide: BorderSide(
            color: color,
            width: 1.0,
          ),
        ),
        focusedBorder: kTextFieldDecoration.focusedBorder?.copyWith(
          borderSide: BorderSide(
            color: color,
            width: 2.0,
          ),
        ),
      ),
    );
  }
}
