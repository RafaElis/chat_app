import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@freezed
abstract class User with _$User {
  const User._();
  factory User({
    required String id,
    required String email,
    required String name,
    required String photoUrl,
    required DateTime lastMessageTime,
  }) = _User;
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  factory User.fromDocument(DocumentSnapshot doc) {
    final data = doc.data() as Map<String, dynamic>;
    return User.fromJson(data).copyWith(id: data['id']);
  }

  Map<String, dynamic> toDocument() => toJson()..remove('id');
}
