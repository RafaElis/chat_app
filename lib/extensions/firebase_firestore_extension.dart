import 'package:cloud_firestore/cloud_firestore.dart';

extension FirebaseFirestoreX on FirebaseFirestore {
  DocumentReference<Map<String, dynamic>> usersListRef(String userId) =>
      collection('users').doc(userId);
}
