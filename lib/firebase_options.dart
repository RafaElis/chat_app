// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyAur5nzTBsMRGlUD9o1hDahzuLsfVF0b3E',
    appId: '1:687902148608:web:7a44564a987be55b8370e5',
    messagingSenderId: '687902148608',
    projectId: 'flash-chat-90147',
    authDomain: 'flash-chat-90147.firebaseapp.com',
    databaseURL: 'https://flash-chat-90147-default-rtdb.firebaseio.com',
    storageBucket: 'flash-chat-90147.appspot.com',
    measurementId: 'G-QEFV5HZ9TF',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyDvxYOsd0MI7_UzzsUA4-vGnV1_EEBxKrw',
    appId: '1:687902148608:android:9a33df332892bf2a8370e5',
    messagingSenderId: '687902148608',
    projectId: 'flash-chat-90147',
    databaseURL: 'https://flash-chat-90147-default-rtdb.firebaseio.com',
    storageBucket: 'flash-chat-90147.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyBcDov1v2Nt8TH9cmpEMAFW0tghevSx5eQ',
    appId: '1:687902148608:ios:857a59049337335a8370e5',
    messagingSenderId: '687902148608',
    projectId: 'flash-chat-90147',
    databaseURL: 'https://flash-chat-90147-default-rtdb.firebaseio.com',
    storageBucket: 'flash-chat-90147.appspot.com',
    androidClientId: '687902148608-oseu7m2hfnr1g0ebfppmr22pj8m48ffe.apps.googleusercontent.com',
    iosClientId: '687902148608-ghlfa9sr2as1bb7k7v0f2nontn385hv5.apps.googleusercontent.com',
    iosBundleId: 'com.rrelias.chatApp',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyBcDov1v2Nt8TH9cmpEMAFW0tghevSx5eQ',
    appId: '1:687902148608:ios:857a59049337335a8370e5',
    messagingSenderId: '687902148608',
    projectId: 'flash-chat-90147',
    databaseURL: 'https://flash-chat-90147-default-rtdb.firebaseio.com',
    storageBucket: 'flash-chat-90147.appspot.com',
    androidClientId: '687902148608-oseu7m2hfnr1g0ebfppmr22pj8m48ffe.apps.googleusercontent.com',
    iosClientId: '687902148608-ghlfa9sr2as1bb7k7v0f2nontn385hv5.apps.googleusercontent.com',
    iosBundleId: 'com.rrelias.chatApp',
  );
}
