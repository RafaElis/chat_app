import 'package:flutter/material.dart';

const kSendButtonTextStyle = TextStyle(
  color: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

const kMessageTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  hintText: 'Type your message here...',
  border: InputBorder.none,
);

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
  ),
);

const kTextFieldStyle = TextStyle(
  color: Colors.black,
);

const kTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(16.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.lightBlueAccent, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(16.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(16.0)),
  ),
);

enum FieldsType {
  email,
  password,
  name,
  phone,
}

Map<String, dynamic> selectFieldType(FieldsType type) {
  switch (type) {
    case FieldsType.email:
      return {
        'icon': Icons.email,
        'keyboardType': TextInputType.emailAddress,
        'obscureText': false,
      };
    case FieldsType.password:
      return {
        'icon': Icons.lock,
        'keyboardType': TextInputType.text,
        'obscureText': true,
      };
    default:
      return {
        'icon': Icons.person,
        'keyboardType': TextInputType.text,
        'obscureText': false,
      };
  }
}

// enum FieldsType {
//   email({
//     'icon': Icons.email,
//     'keyboardType': TextInputType.emailAddress,
//     'obscureText': false,
//   }),
//   password({
//     'icon': Icons.lock,
//     'keyboardType': TextInputType.text,
//     'obscureText': true,
//   });

//   final Map<String, dynamic> options;
//   const FieldsType(this.options);
//   IconData get icon => options['icon'];
//   TextInputType get keyboardType => options['keyboardType'];
//   bool get obscureText => options['obscureText'];
// }