import 'package:intl/intl.dart';

class Utility {
  static String getCurrency(double price) {
    NumberFormat formatter = NumberFormat.simpleCurrency(locale: 'pt_BR');
    return formatter.format(price);
  }

  static String getFormattedDateTime(
    String time, {
    String? pattern = "EEEE, dd 'De' MMMM 'De' y",
  }) {
    DateFormat formatter = DateFormat(pattern, 'pt_Br');
    return formatter.format(DateTime.parse(time));
  }
}
