import 'package:chat_app/controllers/auth_controller.dart';
import 'package:chat_app/firebase_options.dart';
import 'package:chat_app/routers.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  initDeps();
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  runApp(const ProviderScope(child: ChatApp()));
}

void initDeps() async {
  final container = ProviderContainer();
  container.read(authControllerProvider.notifier).appStarted();
}

class ChatApp extends ConsumerWidget {
  final SharedPreferences? prefs;
  const ChatApp({
    this.prefs,
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Intl.defaultLocale = 'pt_BR';
    ref.watch(authControllerProvider.notifier);
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'Biuri Profissional',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [Locale('pt', 'BR')],
      theme: ThemeData(),
      themeMode: ThemeMode.system,
      routeInformationParser: goRouteBuilder(ref).routeInformationParser,
      routerDelegate: goRouteBuilder(ref).routerDelegate,
    );
  }
}
